-- 3.1.1
select last_name from customer 
	where left(lower(last_name), 1) in ('a', 'b','c');
-- 3.1.2
select concat(first_name, ' ', last_name) from customer 
	where active = 1;
-- 3.1.3
select email, customer_id from customer 
	order by char_length(email) 
	limit 10;
-- 3.1.4
select store_id, count(store_id) as 'Anzahl Kunden' from customer 
	group by store_id;
-- 3.1.5
select film_id, title, rental_rate from film 
	where rating = 'G' 
		and rental_rate < 1.0;
-- 3.1.6
select film.film_id, title, Count(inventory.film_id) as 'Verfügbar' from film 
	join inventory on inventory.film_id = film.film_id 
	where inventory.store_id = 1 
	group by film.film_id;
-- 3.1.7
select store_id, rating, count(inventory.film_id) as 'anzahl' from film 
	join inventory on inventory.film_id = film.film_id 
	group by store_id, rating 
	order by store_id, rating ASC;
-- 3.1.8
select film.film_id, film.rating, film.title from film 
	join film_category on film.film_id = film_category.film_id 
	join category on film_category.category_id = category.category_id 
	where category.name = 'Children' 
		and (film.rating = 'R' or film.rating = 'NC-17');
-- 3.1.9
select customer_id, first_name, last_name, email, address, postal_code, city from customer 
	join address on address.address_id = customer.address_id 
	join city on address.city_id = city.city_id 
	join country on city.country_id = country.country_id 
	where country = 'Germany';
-- 3.1.10
select country, count(customer.customer_id) as Kunden from customer 
	join address on address.address_id = customer.address_id 
	join city on address.city_id = city.city_id 
	join country on city.country_id = country.country_id 
	group by country
	having Kunden >= 30
	order by Kunden desc;
