-- 3.1.11
select first_name, last_name, Monthname(payment_date) as month, sum(amount) as charge from payment 
	join customer on payment.customer_id = customer.customer_id
	where payment_date 
		between cast('2005-01-01' as datetime) 
			and cast('2005-06-30' as datetime) 
	group by customer.customer_id
	order by month desc, 
		charge desc;
-- 3.1.12
select monthname(payment_date) as Monat, sum(amount) as Umsatz from payment
	group by year(payment_date), Monat
	order by Umsatz desc
	limit 1;
-- 3.1.13
select category.name as 'Kategorie', inventory.store_id as 'Store', sum(payment.amount) as Umsatz from payment
	join rental on payment.rental_id = rental.rental_id
	join inventory on rental.inventory_id = inventory.inventory_id
	join film on inventory.film_id = film.film_id
	join film_category on film.film_id = film_category.film_id
	join category on film_category.category_id = category.category_id
	group by category.category_id, inventory.store_id;
-- 3.1.14 TODO: Wrong order, wrong persons...
with supervisor as (
	select * from staff
	where staff_id = 6
	union
	select supv.*
	from staff as empl, staff as supv
	where supv.staff_id = empl.supervisor_id)
select first_name, last_name from supervisor;
-- 3.1.15
-- detail-informationen zu den Spalten? Was genau ist damit gemeint???
